package sample

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func NewRouter() *echo.Echo {
	e := echo.New()
	e.GET("/users", userIndexHandler)
	e.GET("/users/:name", userShowHandler)
	e.POST("/users", userCreateHandler)

	return e
}

type User struct {
	Name  string `json:"name" form:"name"`
	Email string `json:"email" form:"email"`
}

type Users []User

func createDummyData() Users {
	var users Users
	users = append(users, User{
		Name:  "taro",
		Email: "taro@example.com",
	})
	users = append(users, User{
		Name:  "jiro",
		Email: "jiro@example.com",
	})
	return users
}

func userIndexHandler(c echo.Context) error {
	users := createDummyData()
	return c.JSON(http.StatusOK, users)
}

func userShowHandler(c echo.Context) error {
	name := c.Param("name")

	users := createDummyData()
	for _, user := range users {
		if user.Name == name {
			return c.JSON(http.StatusOK, user)
		}
	}

	user := new(User)
	return c.JSON(http.StatusNotFound, user)
}

func userCreateHandler(c echo.Context) error {
	user := new(User)
	if err := c.Bind(user); err != nil {
		return err
	}

	return c.JSON(http.StatusCreated, user)
}
